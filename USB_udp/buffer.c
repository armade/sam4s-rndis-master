/*
 * buffer.c
 *
 *  Created on: 02/09/2020
 *      Author: pm
 */
#include "buffer.h"
#include "interrupt_sam_nvic.h"
// Make buffer elements. Remember to call InitJTAG_Queue(), to put them into freebufferqueue
buf_Ele_t scratch[Max_ele];

buf_Ele_t *getElementFromQueue(Queue_t *queue)
{
	irqflags_t flags;
	buf_Ele_t *element, *n;
	asm volatile ("" : : : "memory");
	flags = cpu_irq_save();

	element = queue->remove;             		//do we have an element?
	if (element) {
		queue->ElementCount--;
		queue->remove = (n = element->next); 	//next removal happens at next element,
		if (!n) queue->insert = 0;       		//and if empty, insert null pointer
	}

	asm volatile ("" : : : "memory");
	cpu_irq_restore(flags);

	return element;
}

void putElementIntoQueue(Queue_t *queue, buf_Ele_t *element)
{
	buf_Ele_t *buf;
	irqflags_t flags;
	element->next=0;

	asm volatile ("" : : : "memory");
	flags = cpu_irq_save();
	buf = queue->insert;
	if (buf)
		buf->next = element;
	else
		queue->remove=element;
	queue->ElementCount++;
	queue->insert = element;
	element = 0;
	asm volatile ("" : : : "memory"); //element->next=0 must occur prior to sei
	cpu_irq_restore(flags);
}

void Init_Queue(void)
{
	unsigned char i;

	for (i = 0; i < Max_ele; i++)
		putElementIntoQueue(&freebufqueue, scratch+i);
}


