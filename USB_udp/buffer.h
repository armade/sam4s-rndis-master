#ifndef buffer_H_
#define buffer_H_

#define Max_ele	20

typedef struct T_bufferElement {
	struct T_bufferElement * volatile next;
	struct pbuf *frame;
} buf_Ele_t;

typedef struct {
	buf_Ele_t *volatile remove;
	buf_Ele_t *volatile insert;
	unsigned char ElementCount;
} Queue_t;

Queue_t freebufqueue;
Queue_t rxqueue;
Queue_t txqueue;

buf_Ele_t *getElementFromQueue(Queue_t *queue);
void putElementIntoQueue(Queue_t *queue, buf_Ele_t *element);
void Init_Queue(void);

#endif
