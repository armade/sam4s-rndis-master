/*
    USB descriptor macros for CDC

    Copyright (C) 2015,2016,2018 Peter Lawrence

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the "Software"), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
*/

#ifndef __CDC_HELPER_H
#define __CDC_HELPER_H

#include <stdint.h>


#define CDC_CMD_PACKET_SIZE      8
#define USB_FS_MAX_PACKET_SIZE   64

struct ecm_interface
{
	usb_iface_desc_t             ctl_interface;
	usb_cdc_hdr_desc_t cdc_ecm_header;
	usb_cdc_union_desc_t  cdc_ecm_union;
	cdc_enet_functional_desc_t   cdc_ecm_functional;
	usb_ep_desc_t              ctl_ep;
	usb_iface_desc_t             dat_interface;
	usb_ep_desc_t              ep_in;
	usb_ep_desc_t              ep_out;
};

struct cdc_interface
{
	usb_iad_desc_t 				cdc_association;
	usb_iface_desc_t			ctl_interface;
	usb_cdc_hdr_desc_t 			cdc_acm_header;
	usb_cdc_call_mgmt_desc_t	cdc_cm;
	usb_cdc_acm_desc_t    		cdc_acm;
	usb_cdc_union_desc_t  		cdc_union;
	usb_ep_desc_t              	ctl_ep;
	usb_iface_desc_t     		dat_interface;
	usb_ep_desc_t				ep_in;
	usb_ep_desc_t				ep_out;
};

/* macro to help generate CDC ACM USB descriptors */



#define RNDIS_DESCRIPTOR(COMMAND_ITF, DATA_ITF, COMMAND_EP, DATAOUT_EP, DATAIN_EP) \
    { \
      .cdc_association = { \
        /*Interface Association Descriptor */ \
        .bLength            = sizeof(usb_iad_desc_t), /* Interface Association Descriptor size */ \
        .bDescriptorType    = USB_DT_IAD,            /* Interface Association */ \
        .bFirstInterface    = COMMAND_ITF,                                     /* First Interface of Association */ \
        .bInterfaceCount    = 0x02,                                            /* quantity of interfaces in association */ \
        .bFunctionClass     = 224,                                             /* Wireless */ \
        .bFunctionSubClass  = 0x01,                                            /* Radio Frequency */ \
        .bFunctionProtocol  = 0x03,                                            /* RNDIS */ \
        .iFunction          = 0x00,                                            \
      }, \
 \
      .ctl_interface = { \
        /*Interface Descriptor */ \
        .bLength            = sizeof(usb_iface_desc_t),             /* Interface Descriptor size */ \
        .bDescriptorType    = USB_DT_INTERFACE,                        /* Interface */ \
        .bInterfaceNumber   = COMMAND_ITF,                                     /* Number of Interface */ \
        .bAlternateSetting  = 0x00,                                            /* Alternate setting */ \
        .bNumEndpoints      = 0x01,                                            /* One endpoints used */ \
        .bInterfaceClass    = 224,                                             /* Wireless */ \
        .bInterfaceSubClass = 0x01,                                            /* Radio Frequency */ \
        .bInterfaceProtocol = 0x03,                                            /* RNDIS */ \
        .iInterface         = 0x00,                                            \
      }, \
 \
      .cdc_acm_header = { \
        /*Header Functional Descriptor*/ \
        .bFunctionLength    = sizeof(usb_cdc_hdr_desc_t), /* Endpoint Descriptor size */ \
        .bDescriptorType    = 0x24,                                            /* CS_INTERFACE */ \
        .bDescriptorSubtype = 0x00,                                            /* Header Func Desc */ \
        .bcdCDC             = LE16(0x0110),                              /* spec release number */ \
      }, \
 \
      .cdc_cm = { \
        /*Call Management Functional Descriptor*/ \
        .bFunctionLength    = sizeof(usb_cdc_call_mgmt_desc_t),     \
        .bDescriptorType    = 0x24,                                            /* CS_INTERFACE */ \
        .bDescriptorSubtype = 0x01,                                            /* Call Management Func Desc */ \
        .bmCapabilities     = 0x00,                                            /* D0+D1 */ \
        .bDataInterface     = DATA_ITF,                                        \
      }, \
 \
      .cdc_acm = { \
        /*ACM Functional Descriptor*/ \
        .bFunctionLength    = sizeof(usb_cdc_acm_desc_t),    \
        .bDescriptorType    = 0x24,                                            /* CS_INTERFACE */ \
        .bDescriptorSubtype = 0x02,                                            /* Abstract Control Management desc */ \
        .bmCapabilities     = 0x00,                                            \
      }, \
 \
      .cdc_union = { \
        /*Union Functional Descriptor*/ \
        .bFunctionLength    = sizeof(usb_cdc_union_desc_t),  \
        .bDescriptorType    = 0x24,                                            /* CS_INTERFACE */ \
        .bDescriptorSubtype = 0x06,                                            /* Union func desc */ \
        .bMasterInterface   = COMMAND_ITF,                                     /* Communication class interface */ \
        .bSlaveInterface0   = DATA_ITF,                                        /* Data Class Interface */ \
      }, \
 \
      .ctl_ep = { \
        /* Command Endpoint Descriptor*/ \
        .bLength            = sizeof(usb_ep_desc_t),              /* Endpoint Descriptor size */ \
        .bDescriptorType    = USB_DT_ENDPOINT,                         /* Endpoint */ \
        .bEndpointAddress   = COMMAND_EP,                                      \
        .bmAttributes       = 0x03,                                            /* Interrupt */ \
        .wMaxPacketSize     = LE16(CDC_CMD_PACKET_SIZE),                 \
        .bInterval          = 0x01,                                            \
      }, \
 \
      { \
        /*Data class interface descriptor*/ \
        .bLength            = sizeof(usb_iface_desc_t),             /* Endpoint Descriptor size */ \
        .bDescriptorType    = USB_DT_INTERFACE,                        \
        .bInterfaceNumber   = DATA_ITF,                                        /* Number of Interface */ \
        .bAlternateSetting  = 0x00,                                            /* Alternate setting */ \
        .bNumEndpoints      = 0x02,                                            /* Two endpoints used */ \
        .bInterfaceClass    = 0x0A,                                            /* CDC */ \
        .bInterfaceSubClass = 0x00,                                            \
        .bInterfaceProtocol = 0x00,                                            \
        .iInterface         = 0x00,                                            \
      }, \
 \
      .ep_in = { \
        /* Data Endpoint OUT Descriptor */ \
        .bLength            = sizeof(usb_ep_desc_t),              /* Endpoint Descriptor size */ \
        .bDescriptorType    = USB_DT_ENDPOINT,                         /* Endpoint */ \
        .bEndpointAddress   = DATAOUT_EP,                                      \
        .bmAttributes       = 0x02,                                            /* Bulk */ \
        .wMaxPacketSize     = LE16(USB_FS_MAX_PACKET_SIZE),              \
        .bInterval          = 0x00,                                            /* ignore for Bulk transfer */ \
      }, \
 \
      .ep_out = { \
        /* Data Endpoint IN Descriptor*/ \
        .bLength            = sizeof(usb_ep_desc_t),              /* Endpoint Descriptor size */ \
        .bDescriptorType    = USB_DT_ENDPOINT,                         /* Endpoint */ \
        .bEndpointAddress   = DATAIN_EP,                                       \
        .bmAttributes       = 0x02,                                            /* Bulk */ \
        .wMaxPacketSize     = LE16(USB_FS_MAX_PACKET_SIZE),              \
        .bInterval          = 0x00                                             /* ignore for Bulk transfer */ \
      } \
    },

#endif /* __CDC_HELPER_H */
